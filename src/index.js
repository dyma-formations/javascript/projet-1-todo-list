document.getElementById('focus').focus();

const ul = document.querySelector("ul");
const form = document.querySelector("form");
const input = document.querySelector("form > input");

form.addEventListener("submit", (event) => {
  event.preventDefault();
  const value = input.value;
  input.value = '';

  addTodo(value);

})

const todos = [
  {
    text: 'lorem ipsum',
    done: false,
    editMode: true
  },
  {
    text: 'lorem ipsum',
    done: true,
    editMode: false
  }
]

const displayTodo = () => {

  const todosNode = todos.map((todo, index) => {
    if (todo.editMode) {
      return createTodoEditElement(todo, index)
    } else {
      return createTodoElement(todo, index)

    }
  })

  ul.innerHTML = '';
  ul.append(...todosNode);
}

const createTodoElement = (todo, index) => {
  const li = document.createElement('li');
  const buttonDelete = document.createElement('button');
  buttonDelete.innerHTML = `<i class="fas fa-trash"></i>`
  buttonDelete.className = "icon warning"
  buttonDelete.addEventListener('click', (event) => {
    event.stopPropagation();
    delTodo(index);
  })

  const buttonEdit = document.createElement('button');
  buttonEdit.innerHTML = `<i class="fas fa-edit"></i>`
  buttonEdit.className = "icon primary"
  buttonEdit.addEventListener('click', (event) => {
    event.stopPropagation();
    toggleEditMode(index);
  })

  li.innerHTML = `
    <span class="todo ${todo.done ? "done" : ''}"></span>
    <p>${todo.text}</p>
  `;
  li.addEventListener('click', (event) => {
    toggleTodo(index);
  })
  li.append(buttonEdit, buttonDelete)
  return li;
}

const createTodoEditElement = (todo, index) => {
  const li = document.createElement("li");
  const input = document.createElement("input");
  input.type = "text";
  input.value = todo.text;
  const buttonSave = document.createElement("button");
  buttonSave.innerHTML = "Save";
  buttonSave.className = "btn-primary"

  const buttonCancel = document.createElement("button");
  buttonCancel.innerHTML = "Cancel";

  buttonSave.addEventListener('click', (event) => {
    event.stopPropagation();
    updateTodo(index, input);
  })
  buttonCancel.addEventListener('click', (event) => {
    event.stopPropagation();
    toggleEditMode(index);
  })
  li.append(input, buttonCancel, buttonSave);
  return li
}

const addTodo = (text) => {
  if (text) {
    todos.push({
      text,
      done: false
    })
    displayTodo();
  } else {
    alert("Veuillez entrer un texte")
  }
}

const delTodo = (index) => {
  todos.splice(index, 1);
  displayTodo();
}

const updateTodo = (index, input) => {
  const value = input.value;
  todos[index].text = value;
  toggleEditMode(index);
}

const toggleTodo = index => {
  todos[index].done = !todos[index].done;
  displayTodo();
}

const toggleEditMode = index => {
  todos[index].editMode = !todos[index].editMode;
  displayTodo();
}

displayTodo();